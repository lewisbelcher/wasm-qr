mod dom;
mod qrcode;

use std::panic;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global allocator
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
	// Set panic hook to log rust backtrace to browser console:
	panic::set_hook(Box::new(console_error_panic_hook::hook));

	Ok(())
}

#[wasm_bindgen]
pub fn log_image_qr_data(file_reader: web_sys::FileReader) {
	let div = dom::get_document()
		.get_element_by_id("results")
		.expect("Couldn't get results element");

	let img = match get_image(file_reader) {
		Some(value) => value,
		_ => {
			div.set_inner_html("Couldn't interpret file as an image");
			return;
		}
	};
	let qrcodes = qrcode::extract_all(img);

	div.set_inner_html("");

	for (i, qrcode) in qrcodes.iter().enumerate() {
		web_sys::console::log_1(&JsValue::from_str(&qrcode.to_string()));
		div.append_child(&create_qrcode_header(i))
			.expect("Failed to append child");
		div.append_child(&qrcode.to_element())
			.expect("Failed to append child");
	}
}

fn create_qrcode_header(i: usize) -> web_sys::Element {
	let element = dom::get_document()
		.create_element("p")
		.expect("Failed to create element");
	element.set_class_name("qrcode-header");
	element.set_inner_html(&format!("QR Code {}", i + 1));
	element
}

fn get_image(file_reader: web_sys::FileReader) -> Option<image::DynamicImage> {
	let array_buffer = file_reader
		.result()
		.expect("Couldn't get file_reader result");
	let vec = js_sys::Uint8Array::new(&array_buffer).to_vec();
	match image::load_from_memory(&vec) {
		Ok(value) => Some(value),
		_ => None,
	}
}
