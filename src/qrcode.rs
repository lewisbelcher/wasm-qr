//! # Abstration around a QR Code object.
//!
//! Primary struct is `QRCode`.

use crate::dom;

/// Abstraction of a QR code representation.
pub struct QRCode(quircs::Data);

/// This trait is to simplify the conversion from an `Option<T>` to the `Debug`
/// string of `T` (if `Some`) or the string `"None"`.
trait ToString {
	fn to_string(&self) -> String;
}

impl<T> ToString for Option<T>
where
	T: std::fmt::Debug,
{
	fn to_string(&self) -> String {
		match self {
			Some(v) => format!("{:?}", v),
			None => "None".to_string(),
		}
	}
}

impl QRCode {
	/// Return a `String` representation of `self`.
	pub fn to_string(&self) -> String {
		format!(
			"version: {}; ecc_level: {:?}; mask: {}; data_type: {}; payload: {}; eci: {}",
			self.0.version,
			self.0.ecc_level,
			self.0.mask,
			self.0.data_type.to_string(),
			// TODO: First try to decode UTF8, if it fails show raw bytes
			std::str::from_utf8(&self.0.payload).expect("QR code contained non-UTF8 data"),
			self.0.eci.to_string(),
		)
	}

	/// Return an `Element` representation of `self`.
	pub fn to_element(&self) -> web_sys::Element {
		// TODO: Also get a crop of the QR from the image
		let element = dom::get_document()
			.create_element("table")
			.expect("Failed to create element");
		element.set_class_name("qrcode-table");
		let html: String = self
			.to_string()
			.split("; ")
			.map(|x| {
				let mut split = x.split(": ");
				let key = split.next().unwrap();
				let value = split.next().unwrap();
				format!(
					"<tr><td class=key-column>{}</td><td>{}</td><tr>",
					key, value
				)
			})
			.collect();
		element.set_inner_html(&html);
		element
	}
}

/// Extract all QR codes found within an image.
pub fn extract_all(img: image::DynamicImage) -> Vec<QRCode> {
	let img_gray = img.into_luma8();
	let mut decoder = quircs::Quirc::default();

	let codes = decoder.identify(
		img_gray.width() as usize,
		img_gray.height() as usize,
		&img_gray,
	);

	codes
		.map(|x| {
			QRCode(
				x.expect("failed to extract qr code")
					.decode()
					.expect("failed to decode qr code"),
			)
		})
		.collect()
}
