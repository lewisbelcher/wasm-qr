/// Get the current window's document.
pub fn get_document() -> web_sys::Document {
	web_sys::window()
		.expect("no global `window` exists")
		.document()
		.expect("should have a document on window")
}
